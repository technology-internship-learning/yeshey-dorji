<style>
  body {
    background-color: white;
  }
</style>
<style>
  body {
    color: black;
  }
</style>

# Electronic Designing
![Image](../Image/Circuit.jpg)

## First Learning Experience
During our first learning experience, we explored the concept of microcontrollers, which serve as the brains of electronic circuits, controlling and coordinating their functions. One microcontroller we focused on was the ATtiny44. It acts like a tiny computer, managing various tasks within the circuit, such as receiving input from sensors, controlling output to devices like motors or lights, and processing data. To apply what we learned, we used KiCad, a software tool for designing electronic circuits, to create a footprint for our circuit board. This involved digitally representing the physical layout and pin configuration of the ATtiny44 on the board, ensuring it fits correctly and can connect to other components as needed. Through this practical exercise, we gained hands-on experience in electronics design and learned how to translate theoretical knowledge into real-world applications. By mastering tools like KiCad and understanding the role of microcontrollers like the ATtiny44, we're taking important steps towards becoming proficient in electronics and embedded systems development. Also, I learned a basic about schematic. 

### Schematic
 I compiled the components included in the fab academy guide line. I've got all these components spread out in front of me, but I haven't quite figured out how they all fit together yet. It's like having a bunch of puzzle pieces but not knowing how they make the picture. I know I need to draw up a schematic, which is like a map that shows how all the parts connect. But right now, I've only got the list of parts checked off. I haven't started drawing the lines to show how they link up. It's like I've gathered all the ingredients for a recipe, but I haven't started cooking yet. So, my next step is to take those parts and start connecting them on the schematic. That way, I'll have a clear plan for how to put everything together and make my project work!


[<span style="color: black;"><u><b>About ATtiny 44</span></b></u>](./ATtiny44.md) 


## Second and Third Learning Experience
During our learning, we got to do some hands-on work by soldering components onto a circuit board. These components, like capacitors, resistors, and buttons, are like the tiny building blocks that make up electronic devices. Soldering is like gluing them onto the circuit board so they stay in place and can connect to each other properly. Even though we didn't get to use the SAM-20 machine, we learned about how it works and what it's used for. It's a machine that helps put tiny electronic parts onto the circuit board really accurately and quickly. We also learned about the materials used during soldering. Solder is like a special metal glue that melts when heated and then hardens to hold everything together. And flux is like a cleaning agent that helps the solder stick better and makes sure the connections are strong and clean. So, even though we couldn't use all the equipment, we still got to learn a lot about how electronic devices are put together and how soldering works! 
<br>
<br>

<img src="../Image/Resistor.jpg" alt="Your Image" style="width:450px;height:300px;">

<br>
<br>
This is Ngawang Pemo Dukpa's circuit board. I tried soldering two resistors and an ATtiny44 microcontroller onto it. Resistors are like tiny traffic controllers in a circuit, controlling the flow of electricity. The ATtiny44 microcontroller, on the other hand, is like the brain of the whole operation, telling the circuit what to do. Soldering is like using a special glue to attach these parts onto the circuit board so they stay in place and can talk to each other properly. It's a bit like using a glue gun but with melted metal instead of glue. Even though it might be tricky at first, with practice and patience, I'll soon get the hang of soldering

<br>
<br>


<img src="../Image/Button.jpg" alt="Your Image" style="width:450px;height:300px;">

<br>
<br>


Damzang Chimi's circuit board is where I attempted to install a push button. A push button is a small component that acts like a switch, allowing users to control various functions in electronic devices by pressing it. It's a crucial part of many gadgets, used for tasks like turning devices on or off, making selections, or triggering actions.